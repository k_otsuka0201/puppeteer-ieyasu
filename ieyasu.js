const puppeteer = require('puppeteer');
const Moment = require('moment');
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);
const prompts = require('prompts');
const fs = require('fs');
require('dotenv').config();

// IEYASUのURLとログインIDとPWを指定
const IEYASU_URL = 'https://f.ieyasu.co/daisho_/login';
const USER_IEYASU = process.env.USER_IEYASU;
const PASS_IEYASU = process.env.PASS_IEYASU;

//
let IEYASU_MODE = null;
if (!process.argv[2]) {
    // TODO 入力待ちにならない
    // (async () => {
    //     let question_ieyasumode = [
    //         {
    //             type: 'select',
    //             name: 'mode',
    //             message: '出勤 or 退勤 ?',
    //             choices: [
    //                 {title: '出勤', value: 'starting'},
    //                 {title: '退勤', value: 'ending'}
    //             ]
    //         }
    //     ];
    //     let answer = await prompts(question_ieyasumode);
    //     IEYASU_MODE = answer.mode;
    // })();
    console.log('startingかendingを引数として指定してください');
    return false;
} else {
    try {
        if (['starting', 'ending'].indexOf(process.argv[2]) >= 0) {
            IEYASU_MODE = process.argv[2];
        } else {
            throw new Error('IEYASUモードの指定が不正です');
        }
    } catch (e) {
        console.log(e.message);
        return false;
    }
}

// 出力するファイルのパスを指定
const OUTPUT_PATH = './outputs/';

// ファイル名（例 : 2020年4月5日(日) 16:21）を定義
moment.locale('ja');
const FILENAME = moment().format('llll');

// 起算日（毎月16日）からの経過日数を取得
const today_month = moment().month();
const today_date = moment().date();
let start = null;
if (today_date < 16) {
    const last_month = moment().subtract(1, 'months');
    start = new Date(last_month.year(), last_month.month(), 15);
} else {
    start = new Date(moment().year(), moment().month(), 15);
}
const end   = new Date();
const range = moment.range(start, end);
const range_days = range.diff('days');

(async() => {
    const browser = await puppeteer.launch({
        args: [
            '--no-sandbox',
            '--disable-setuid-sandbox'
        ]
    });
    const page = await browser.newPage();
    await page.setViewport({
        width: 1280,
        height: 960
    });

    // ログイン
    await page.goto(IEYASU_URL);
    await page.type('input#user_login_id', USER_IEYASU);
    await page.type('input#user_password', PASS_IEYASU);
    page.click('input[name="Submit"]');
    await page.waitForNavigation({
        waitUntil: 'networkidle0'
    });

    // 出勤を打刻
    // TODO 出勤or退勤の処理が完了するのを検出
    const button_selector = (IEYASU_MODE === 'starting') ? 'a#btnIN1' : 'a#btnIN2' ;
    await page.click(button_selector);
    await page.waitForResponse(response => {
        console.log(response.status());
        return true;
    });

    await page.waitFor(1000);

    // 日次勤怠に移動
    page.click('a#work_navi');
    await page.waitForNavigation({
        waitUntil: 'networkidle0'
    });

    let elementDates = await page.$$('table#editGraphTable tbody tr');
    let elementDate = await elementDates[range_days - 1];
    await elementDate.screenshot({
        path: OUTPUT_PATH + FILENAME + '.png'
    });

    const scrapingData = await page.evaluate((range_days) => {
        let elementDate = document.querySelector('table#editGraphTable tbody tr:nth-child(' + range_days + ')');

        return datalist = {
            'date': elementDate.querySelector('.date').innerText,
            'day': elementDate.querySelector('.day').innerText,
            'type01': elementDate.querySelector('.cellType .item01').innerText,
            'type02': elementDate.querySelector('.cellType .item02').innerText,
            'start01': elementDate.querySelector('.cellTime01 .item01').innerText,
            'start02': elementDate.querySelector('.cellTime01 .item02').innerText,
            'end01': elementDate.querySelector('.cellTime02 .item01').innerText,
            'end02': elementDate.querySelector('.cellTime02 .item02').innerText,
        };
    }, range_days);

    console.log(scrapingData.date + '(' + scrapingData.day + ')');
    console.log('勤務区分: ' + scrapingData.type01 + '(' + scrapingData.type02 + ')');
    console.log('出勤時間: ' + scrapingData.start01 + '(打刻時間: ' + scrapingData.start02 + ')');
    console.log('退勤時間: ' + scrapingData.end01 + '(打刻時間: ' + scrapingData.end02 + ')');

    fs.writeFile(
        OUTPUT_PATH + FILENAME + '.log',
        JSON.stringify(scrapingData),
        (err) => { if (err) throw err; console.log('done'); }
    );

    browser.close();
})();