# Puppeteer IEYASU



## Installation

Clone any git repository

```bash
$ git clone https://k_otsuka0201@bitbucket.org/k_otsuka0201/puppeteer-ieyasu.git
```

Install node packages.

```bash
$ cd ./puppeteer-ieyasu
$ yarn install
```

Set `.env`

```bash
.env

USER_IEYASU='<Your IEYASU Username>'
PASS_IEYASU='<Your IEYASU Password>'
```

Execute `node ieyasu.js`

```bash
$ node ieyasu.js starting
    or
$ node ieyasu.js ending
```

## Dependencies

- [dotenv](https://www.npmjs.com/package/dotenv)
- [Moment.js](https://momentjs.com/docs/)
- [moment-range](https://www.npmjs.com/package/moment-range)
- [prompts](https://www.npmjs.com/package/prompts)
- [Puppeteer](https://pptr.dev/)